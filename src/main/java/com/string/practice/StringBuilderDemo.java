package com.string.practice;

public class StringBuilderDemo {

	public static void main(String[] args) {

		StringBuilder sb = new StringBuilder("Hava a nice day");

		sb.append(" Good Morning");
		System.out.println(sb);

		sb.deleteCharAt(5);
		System.out.println(sb);

		 sb.reverse();
		 System.out.println(sb);

		sb.replace(5, 8, "java");//including 0,excluding 8
		System.out.println(sb);

		System.out.println("---------------------------------");

		StringBuffer sb1 = new StringBuffer("HelloWorld");
		sb1.insert(2, 'L');
		System.out.println(sb1);

	}

}
