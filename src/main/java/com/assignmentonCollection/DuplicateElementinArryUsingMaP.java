package com.assignmentonCollection;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class DuplicateElementinArryUsingMaP {

	public static void main(String[] args) {

		String name[] = { "apple", "Mango", "Mango", "orange", "fig", "apple", "watermelon", "fig", "apple" };

		Map<String, Integer> map = new HashMap<String, Integer>();

		for (String fruits : name) {

			Integer count = map.get(fruits);
			if (count == null) {

				map.put(fruits, 1);
			} else {

				map.put(fruits, 1 + count);
			}
		}

		Set<Entry<String, Integer>> entrySet = map.entrySet();
		for (Entry<String, Integer> entery : entrySet) {

			if (entery.getValue() > 1) {

				System.out.println(entery.getKey() + " :" + entery.getValue());
			}
		}

	}

}
