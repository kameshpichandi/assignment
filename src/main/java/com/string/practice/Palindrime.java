package com.string.practice;

import java.util.Scanner;

public class Palindrime {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the String......");
		String str1 = sc.nextLine();

		StringBuffer sb = new StringBuffer(str1);
		sb.reverse();
		String str2 = sb.toString();

		if (str1.equals(str2)) {

			System.out.println("String is palindrime");
		}

		else {

			System.out.println("String is not Palindrime");
		}

	}

}
