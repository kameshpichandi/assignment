package com.arraylist.practice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.ListIterator;

public class ArrayListDemo {

	public static void main(String[] args) {

		ArrayList<String> list = new ArrayList<String>();

		list.add("Apple");
		list.add("Mango");
		list.add("Orange");
		list.add("Grape");
		list.add("Apple");
		list.add("Mango");

		Collections.sort(list);
		System.out.println(list);
		System.out.println(list.contains("Orange"));
		list.set(2, "Banana");// To update the element in the particular index

		System.out.println(list.remove(3));
		System.out.println(list);

		ListIterator<String> itr = list.listIterator();
		while (itr.hasNext()) {

			System.out.println(itr.next());
		}
		ArrayList<String> arr1 = new ArrayList<String>();
		arr1.add("Lemon");
		arr1.add("Cherry");
		// list.addAll(arr1);//we can add a list over another list
		System.out.println(list);

		list.addAll(2, arr1);// we can add the list based on the index
		System.out.println(list);

		// arr1.clear(); tTo clear the element in the list

		ArrayList<String> cloneList = (ArrayList<String>) arr1.clone();
		System.out.println(cloneList);

		System.out.println(list.indexOf("Grape"));

		System.out.println("--------------------------------------------------");
		for (int i = 0; i < list.size(); i++) {

			System.out.println(list.get(i));

		}

		int lastIndexOf = list.lastIndexOf("Mango");
		System.out.println(lastIndexOf);
		
		ArrayList<Integer>number=new ArrayList<Integer>(Arrays.asList(1,2,3,4,5,6,7,8,9));
		number.removeIf(num -> num%2==1);
		System.out.println(number);
	}

}
