package com.assignmentonarray;

import java.util.Arrays;

public class FindTheLargestAndSmallestValueInArray {

	public static void main(String[] args) {

		int[] num = { 89, 12, 76, 42, 65, 32, -89, -3, 178, 90, -789 };

		int largest = num[0];
		int smallest = num[0];

		for (int i = 0; i < num.length; i++) {

			if (num[i] > largest) {

				largest = num[i];
			}

			else if (num[i] < smallest) {

				smallest = num[i];
			}
		}

		System.out.println(" given array is: " + Arrays.toString(num));
		System.out.println("The largest Array is :" + largest);
		System.out.println("The smallest Array is :" + smallest);

	}

}
