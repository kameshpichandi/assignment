package com.day1;

public class InheritanceDemo2 {

	public static void main(String[] args) {

		SBI sb = new SBI();
		sb.branchName();
		sb.accountNumber();
		sb.debitCard();

		System.out.println(" --------------------");

		AxisBank ax = new AxisBank();
		ax.showBranch();
		ax.branchName();
		ax.accountNumber();
		ax.debitCard();
		ax.accountHolderName();
		ax.rateOfIntrest();

		System.out.println("------------------------");

		IndianBank ib = new IndianBank();
		ib.branchName();
		ib.accountNumber();
		ib.debitCard();
		ib.accountHolderName();
		ib.rateOfIntrest();
		ib.accountBalance();
		ib.emiBillGeneration();

		
		SBI sb1=new IndianBank();
		sb1.accountNumber();
		sb1.branchName();
		sb1.debitCard();
		
		
		
	}

}
