package com.practices.day1;

import java.util.ArrayList;
import java.util.Iterator;

public class ArrayListDemo {

	public static void main(String[] args) {

		ArrayList<String> list = new ArrayList<String>();
		list.add("kamesh");
		list.add("Kanna");
		list.add("Ramesh");
		list.add("Raghu");
		list.add("pradeep");

		Iterator<String> itr = list.iterator();
		while (itr.hasNext()) {

			System.out.println(itr.next());
			String next = itr.next();
		}

	}

}
