package com.arraylist.practice;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class DuplicateCharacterFromString {

	public static void main(String[] args) {

		String name = "Welcom to Global logic";

		char[] words = name.toCharArray();
		HashMap<Character, Integer> hm = new HashMap<Character, Integer>();

		for (Character ch : words) {

			if (hm.containsKey(ch)) {
				hm.put(ch, hm.get(ch) + 1);

			} else {

				hm.put(ch, 1);
			}
		}

		Set<Map.Entry<Character, Integer>> entrySet = hm.entrySet();
		for (Entry<Character, Integer> entery : entrySet) {

			if (entery.getValue() > 1) {

				System.out.println(entery.getKey() + " :" + entery.getValue());
			}
		}

	}

}
