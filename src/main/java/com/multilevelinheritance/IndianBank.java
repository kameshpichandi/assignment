package com.multilevelinheritance;

public class IndianBank extends StateBank {

	public void accountNumber() {

		System.out.println("Account Number is : 12345668900");
	}

	public void ifscCode() {

		System.out.println("IFSC NO: INB12908U2");
	}

	public static void main(String[] args) {

		// Child class object
		IndianBank ib = new IndianBank();
		ib.rateOfintrest();
		ib.principleRate();
		ib.branchName();
		ib.debitCard();
		ib.accountNumber();
		ib.ifscCode();

		System.out.println("--------------------------------");
		// parent class object

		StateBank ab = new StateBank();
		ab.rateOfintrest();
		ab.principleRate();
		ab.branchName();
		ab.debitCard();

		// Grand parent class

		System.out.println("-------------------------------");
		ReserveBank rb = new ReserveBank();
		rb.rateOfintrest();
		rb.principleRate();

	}

}
