package com.practices.hierachicalinheritance;

public class VegFood extends Food {
	
	
	public void northIndianFood() {
		
		System.out.println("chole bhature");
	}
	
	
	public static void main(String[] args) {
		
		
		
		//Child2 object
		VegFood vg=new VegFood();
		vg.vegitarienFood();
		vg.nonVegitarienFood();
		vg.northIndianFood();
		System.out.println("--------------------------------------");
		
		//Child 1 Object
		Non_VegFood nv=new Non_VegFood();
		nv.chineseFood();
		nv.sounIndianFood();
		nv.vegitarienFood();
		nv.nonVegitarienFood();
		
		System.out.println("--------------------------------");
		//parent object
		Food fd=new Food();
		fd.vegitarienFood();
		fd.nonVegitarienFood();
				
				
		
	}

}
