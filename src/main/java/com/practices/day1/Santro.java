package com.practices.day1;

public class Santro extends Car implements BasicCar {
	
	
	
	public void remoteStart() {
		
		
		System.out.println("Change the driver mode to Automatic mode");
	}

	@Override
	public void gearChange() {
		System.out.println("Change the 1st gear to 2n gear");		
	}

	@Override
	public void music() {
		
		System.out.println("Switch on the Fm in the car");
		
	}
	
	
	
	public static void main(String[] args) {
		Santro st=new Santro();
		st.drive();
		st.stop();
		st.remoteStart();
		st.gearChange();
		st.music();
		
	}
	
	
	
	
	
	
	
	
	

}
