package com.day1;

public class InneritanceDemo1 {

	public static void main(String[] args) {
		
		ChildClass ch=new ChildClass();
		ch.propertyOne();
		ch.propertyTwo();
		
		System.out.println("------------------------");
		
		
		ParentClass pc=new ParentClass();
		pc.propertyOne();
		
		
		
		System.out.println("-------------------------");
		
		
		ParentClass pr=new ChildClass();
		pr.propertyOne();
		
		

	}

}
