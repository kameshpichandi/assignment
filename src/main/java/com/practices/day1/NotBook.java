package com.practices.day1;

public class NotBook extends Book {

	@Override
	public void write() {
		System.out.println("Write the importan Question in the Book");
	}

	@Override
	public void read() {

		System.out.println("Read the importan question in the Book");

	}

	public void draw() {

		System.out.println("Draw the Diagram in the note Book");
	}

	public static void main(String[] args) {

		NotBook nb = new NotBook();
		nb.write();
		nb.read();
		nb.draw();

	}

}
