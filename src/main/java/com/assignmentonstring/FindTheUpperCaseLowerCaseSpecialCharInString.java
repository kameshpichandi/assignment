package com.assignmentonstring;

public class FindTheUpperCaseLowerCaseSpecialCharInString {

	public static void main(String[] args) {

		String str = "WelCome To Global$Logic$123";
		int upperCaseCount = 0;
		int lowerCaseCount = 0;
		int digit = 0;
		int speciaChar = 0;

		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);
			if (Character.isUpperCase(ch)) {
				upperCaseCount++;
			} else if (Character.isLowerCase(ch)) {
				lowerCaseCount++;
			} else if (Character.isDigit(ch)) {
				digit++;
			} else {
				speciaChar++;
			}

		}
		System.out.println("Upper Case Count :" + upperCaseCount);
		System.out.println("lower Case Count :" + lowerCaseCount);
		System.out.println("digit Case Count :" + digit);
		System.out.println("Speial char Count :" + speciaChar);

	}

}
