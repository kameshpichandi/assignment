package com.practices.day1;

public class StudentData {

	private int student_Id;
	private String student_name;
	private int student_age;
	private int student_standend;

	public StudentData() {
		student_Id = 1009;
		student_name = "kannan";
		student_age = 19;
		student_standend = 12;

	}

	public StudentData(int student_Id, String student_name, int student_age, int student_standend) {
		super();
		this.student_Id = student_Id;
		this.student_name = student_name;
		this.student_age = student_age;
		this.student_standend = student_standend;
	}

	public void nameOftheSchool() {

		System.out.println("Green Park CBSC school");
	}

	public int getStudent_Id() {
		return student_Id;
	}

	public void setStudent_Id(int student_Id) {
		this.student_Id = student_Id;
	}

	public String getStudent_name() {
		return student_name;
	}

	public void setStudent_name(String student_name) {
		this.student_name = student_name;
	}

	public int getStudent_age() {
		return student_age;
	}

	public void setStudent_age(int student_age) {
		this.student_age = student_age;
	}

	public int getStudent_standend() {
		return student_standend;
	}

	public void setStudent_standend(int student_standend) {
		this.student_standend = student_standend;
	}

	public static void display() {

		System.out.println("Welcom to School");

	}

	@Override
	public String toString() {
		return "StudentData [student_Id=" + student_Id + ", student_name=" + student_name + ", student_age="
				+ student_age + ", student_standend=" + student_standend + "]";
	}

}
